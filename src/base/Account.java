package base;

import java.util.Date;

public class Account {
	protected int Id;
	protected Date CreationDate;
	protected double Balance;
	protected int UserId;
	
	public Account() {
		super();
	}
	public Account(int id, Date creationDate, double balance, int userId) {
		super();
		Id = id;
		CreationDate = creationDate;
		Balance = balance;
		UserId = userId;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public Date getCreationDate() {
		return CreationDate;
	}
	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}
	public double getBalance() {
		return Balance;
	}
	public void setBalance(double balance) {
		Balance = balance;
	}
	public int getUserId() {
		return UserId;
	}
	public void setUserId(int userId) {
		UserId = userId;
	}
}
