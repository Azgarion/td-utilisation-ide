package base;

public class Customer {
	private int Id;
	private String Name;
	private String Email;
	private int AccountId;
	
	public Customer() {
		super();
	}
	
	public Customer(int id, String name, String email, int accountId) {
		super();
		Id = id;
		Name = name;
		Email = email;
		AccountId = accountId;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public int getAccountId() {
		return AccountId;
	}
	public void setAccountId(int accountId) {
		AccountId = accountId;
	}
}
