package base;

import java.util.Date;

public class CurrentAccount extends Account {
	
	private double Overdraft;
	
	public CurrentAccount(int id, Date creationDate, double balance, int userId, double overdraft) {
		Id = id;
		CreationDate = creationDate;
		Balance = balance;
		UserId = userId;
		Overdraft = overdraft;
	}

	public double getOverdraft() {
		return Overdraft;
	}

	public void setOverdraft(double overdraft) {
		Overdraft = overdraft;
	}

}