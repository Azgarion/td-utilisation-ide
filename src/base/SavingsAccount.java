package base;

import java.util.Date;

public class SavingsAccount extends Account {
	
	private double Rate;
	
	public SavingsAccount(int id, Date creationDate, double balance, int userId, double rate) {
		Id = id;
		CreationDate = creationDate;
		Balance = balance;
		UserId = userId;
		Rate = rate;
	}

	public double getRate() {
		return Rate;
	}

	public void setRate(double rate) {
		Rate = rate;
	}

}
